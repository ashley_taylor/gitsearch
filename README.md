# README #

### What is this repository for? ###

* This repo is for quick and easy browsing of git repositories.
* Version: Beta 0.1.1

### How do I get set up? ###

* Just download and open the index file and you'll be up and running!

### Contribution guidelines ###

* You can modify as you wish and submit your code.
* Try to ensure all functionality has been tested before you submit.
* If you feel you want to add a module or setup npm, please on commit describe the new module and how it will be used.

### Who do I talk to? ###

* You can always talk to me if you need any further information.