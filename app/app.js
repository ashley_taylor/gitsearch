

angular.module('app', [])
    .controller('gitHubDataController', ['$scope','$http', function($scope,$http) {

        // Set the data loading to false on initial load
        $scope.reposLoaded = false;

        $scope.userLoaded = false;

        // Assign a default user
        $scope.username = "JeffreyWay";

        // Retrieve user data
        $http.get("https://api.github.com/users/" + $scope.username)
            .success(function (data) {
                $scope.userData = data;
                loadRepos();
            });

        // Load user repo data
        var loadRepos = function () {
            $http.get($scope.userData.repos_url)
                .success(function (data) {
                    $scope.repoData = data;
                });
        };

        // Refresh username if the user you search for exists
        $scope.loadData = function () {
            $http.get("https://api.github.com/users/" + $scope.username)
            .success(function (data) {
            $scope.userData = data;
            loadRepos();
            });
        };

        $scope.predicate = '-updated_at';
}]);
